import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import Favorites from '../Favorites';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };
//is state the state of a fliped image i
  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size: 200,
      isFliped:true,
      isOpen:false 
    };
    this.flipimage = this.flipimage.bind(this);//binding the function flip image to this class
    this.expandImage = this.expandImage.bind(this);//binding the expand image 
    this.favImage = this.favImage.bind(this) //binding the favorite to image
  }

  /*the function set state between fliped and with not  */
  flipimage(){
    this.setState({isFliped: !this.state.isFliped})
  }
  /*the function set state between expanded and with not  */
  expandImage(){
    this.setState({ isOpen: !this.state.isOpen });
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }
  /*this function is for calling the favorite from favorite .js */
  favImage() {
    Favorites.addToFavorites(this.props.dto);
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  render() {
    let flipClass = this.state.isFliped ? '': ' flip';//variable that concate the class name to the image root  if fliped or not 
    let extandClass = this.state.isOpen ? ' open': '';//variable that concate the class name to the image root  if expanded fliped or not 
    return (
      <div
        className={'image-root' + flipClass +extandClass}
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px'
        }}
        >
        <div className="btns">
          <FontAwesome className="image-icon" name="arrows-alt-h" title="flip" onClick={this.flipimage}/>
          <FontAwesome className="image-icon" name="clone" title="clone" onClick={() => this.props.cloneimage(this.props.dto)}/>
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={this.expandImage}/>
          <FontAwesome className="image-icon" name="heart" title="heart" onClick={this.favImage}/>
        </div>
      </div>
    );
  }
}

export default Image;
