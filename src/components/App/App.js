import React from 'react';
import './App.scss';
import Gallery from '../Gallery';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'sea',
      favoritesOn: false
    };
  }

  render() {
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <input className="app-input" onChange={event => this.setState({tag: event.target.value})} value={this.state.tag}/>
          <button className="fav-Btn" onClick={() => this.setState({favoritesOn: !this.state.favoritesOn})}>favorites</button>
        </div>
        <Gallery tag={this.state.tag} favoritesOn={this.state.favoritesOn} />
      </div>
    );
  }
}

export default App;
