import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import Favorites from '../Favorites';
import './Gallery.scss';

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth()
    };
    // binding the function clone image to this class 
    this.cloneimage = this.cloneimage.bind(this);
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getImages(tag) {
    if(this.props.favoritesOn) {// check the state of gallery  if favorie on showing the array from storge via jason else show the regular gallery 
      this.setState({images: Favorites.getFavorites()});
    } else {
      const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&format=json&safe_search=1&nojsoncallback=1`;
      const baseUrl = 'https://api.flickr.com/';
      axios({
        url: getImagesUrl,
        baseURL: baseUrl,
        method: 'GET'
      })
        .then(res => res.data)
        .then(res => {
          if (
            res &&
            res.photos &&
            res.photos.photo &&
            res.photos.photo.length > 0
          ) {
            this.setState({images: res.photos.photo});
          }
        });
    }
  }

  componentDidMount() {
    this.getImages(this.props.tag);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillReceiveProps(props) {
    this.getImages(props.tag);
  }

  cloneimage(imageDto){
    this.setState({images: [imageDto, ...this.state.images]});//taking an array dto and placing the clone above the array op pictures

  }

  render() {
    return (
      <div className="gallery-root">
        {this.state.images.map((dto, index) => {
          return <Image key={'image-' + index} dto={dto} galleryWidth={this.state.galleryWidth} cloneimage={this.cloneimage}/>;
        })}
      </div>
    );
  }
}

export default Gallery;
