
class Favorites {
    static addToFavorites(obj) {
        const storage = JSON.parse(localStorage.getItem('favorites')) || [];//store an jason array element to the local storge
        if(storage.findIndex(i => i.id == obj.id) == -1) {//chack if in favorite already 
            storage.push(obj);//if not push to the array
            localStorage.setItem('favorites', JSON.stringify(storage));//add the stringfy json to the local storge
        }
    }

    static getFavorites() {
        return JSON.parse(localStorage.getItem('favorites'));//paraseing the json from the storge
    }
}

export default Favorites;